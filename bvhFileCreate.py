
def open_file(file_name):
    file = open(file_name , "w+")
    return file

def close_file(file):
    file.close()

def write_skeleton(file):
    file.write("""HIERARCHY
ROOT Hips
{
	OFFSET	0.00	0.00	0.00
	CHANNELS 6 Xposition Yposition Zposition Zrotation Xrotation Yrotation
	JOINT Chest
	{
		OFFSET	0.00	4.21	0.00
		CHANNELS 3 Zrotation Xrotation Yrotation
		JOINT Neck
		{
			OFFSET	0.00	17.08	0.00
			CHANNELS 3 Zrotation Xrotation Yrotation
			JOINT Head
			{
				OFFSET	0.00	3.97	0.00
				CHANNELS 3 Zrotation Xrotation Yrotation
				End Site 
				{
					OFFSET	0.00	3.23	0.00
				}
			}
		}
		JOINT LeftCollar
		{
			OFFSET	1.02	14.86	1.71
			CHANNELS 3 Zrotation Xrotation Yrotation
			JOINT LeftUpArm
			{
				OFFSET	5.45	0.00	0.00
				CHANNELS 3 Zrotation Xrotation Yrotation
				JOINT LeftLowArm
				{
					OFFSET	0.00	-11.29	0.00
					CHANNELS 3 Zrotation Xrotation Yrotation
					JOINT LeftHand
					{
						OFFSET	0.00	-8.34	0.00
						CHANNELS 3 Zrotation Xrotation Yrotation
						End Site 
						{
							OFFSET	0.00	-6.13	0.00
						}
					}
				}
			}
		}
		JOINT RightCollar
		{
			OFFSET	-1.02	14.86	1.71
			CHANNELS 3 Zrotation Xrotation Yrotation
			JOINT RightUpArm
			{
				OFFSET	-5.52	0.00	0.00
				CHANNELS 3 Zrotation Xrotation Yrotation
				JOINT RightLowArm
				{
					OFFSET	0.00	-10.77	0.00
					CHANNELS 3 Zrotation Xrotation Yrotation
					JOINT RightHand
					{
						OFFSET	0.00	-7.98	0.00
						CHANNELS 3 Zrotation Xrotation Yrotation
						End Site 
						{
							OFFSET	0.00	-6.40	0.00
						}
					}
				}
			}
		}
	}
	JOINT LeftUpLeg
	{
		OFFSET	2.99	0.00	0.00
		CHANNELS 3 Zrotation Xrotation Yrotation
		JOINT LeftLowLeg
		{
			OFFSET	0.00	-15.03	0.00
			CHANNELS 3 Zrotation Xrotation Yrotation
			JOINT LeftFoot
			{
				OFFSET	0.00	-15.42	0.00
				CHANNELS 3 Zrotation Xrotation Yrotation
				End Site 
				{
					OFFSET	0.00	-2.17	0.00
				}
			}
		}
	}
	JOINT RightUpLeg
	{
		OFFSET	-2.57	-0.21	0.00
		CHANNELS 3 Zrotation Xrotation Yrotation
		JOINT RightLowLeg
		{
			OFFSET	0.00	-15.37	0.00
			CHANNELS 3 Zrotation Xrotation Yrotation
			JOINT RightFoot
			{
				OFFSET	0.00	-14.84	0.00
				CHANNELS 3 Zrotation Xrotation Yrotation
				End Site 
				{
					OFFSET	0.00	-2.20	0.00
				}
			}
		}
	}
}"""
)


def write_data(file):
    file.write("""
MOTION
Frames:    181
Frame Time: 0.033333
""" )
    datafile=open("data.txt" , "r+")
    data=datafile.read()
    split_data=data.split("\n")
    length=len(split_data)
    for j in range (length-1):
        sel=split_data[j]
        sel_list=sel.split("    ")
        data= sel_list[0]+"    "+ "0.00    "+sel_list[1]+"    "+sel_list[2]
        file.write( "0.00	32.90	0.00	0.00	-12.00	0.00	0.00	17.08	0.00	0.00	3.97	0.00	0.00	3.23	0.00	1.02	14.86	1.71	0.00	-12.00   " + data + "    0.00	0.00	0.00	0.00	0.00	0.00   -10.52	-20.00   160.00	 0.00	 90.77	0.00	0.00	-7.98	0.00	0.00	-90.00	-5.00	0.00	70.00	0.00	0.00	0.00	0.00     0.00	-90.00	5.00	0.00	70.00	0.00	0.00	0.00	0.00"+"\n")
    close_file(datafile)

    	
    


h=open_file("test.bvh")
write_skeleton(h)
write_data(h)
close_file(h)
