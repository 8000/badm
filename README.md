**BADM -Blast Assessment Demo Modeller**

The BADM  is a human structural modeller embedded with sensors which can be used to identify and measure effect on the human body at armerd vechicle testing. For Sri lanka Army, there is no standrad device or method to accomplish the above tast. To fullfil the above requiremnet the BADM is designed and implemented. The Armred vechicles are tested under the NETO Strandard, STANAG 9569. Therefore the BADM is desined in a way satistfying the STANAG 9569. 


The BADM is embedded with different type of sensors sufficient resolutions. They are place at the body part which have a considerable possibility of having effect on a explosion test on armerd vehicles. All the sensors are controlled the main processor unit, Respberry Pi3 module. A camera is also comes with BADM and provide a video of movement of the modeller. During the explosion the data which are provided by the sensors will store into the external memory. After the test is perform the data will be analysis and calculated effect on the human bady will be provided. 


To measure the movements, orientation and the acceleration on body parts during a explosion, 10 MPU-9255 gyro sensors are attached to the modellers body. 2 sensors on each leg and arm, 1 senser on body and last sensor on head.


The 3D modeling part suppose to be done by the software called as 'Blender'. After the explosion test performance, all necessary files which are need to perform the 3D modeling auomatically craete inside the raspberrypi which is act as the main unit processor. Therefore the user can get the 3D movements of the modeller during the exploasion by coping the file from the SD card to the computer open the file through the Blender software. As the user does not need to worry about file creating and managing, the BADM is very user-friendly and anyone who does not have technical knowledge
can easily handle the BADM.